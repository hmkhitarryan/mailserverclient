package com.egs.receiver;

import com.egs.constant.MailTemplate;
import com.egs.model.MailData;
import com.egs.receiver.listener.RabbitMessageListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ReceiverTest {
    private static final String FROM = "haykmkhitaryan84@gmail.com";
    @Autowired
    RabbitMessageListener rabbitMessageListener;

    @Test
    public void testReceiver() {
        final List<String> toes = new ArrayList<>();
        toes.add("hayk_84@mail.ru");
        final MailData mailData = mailDataBuilder(getDate(), MailTemplate.REGISTRATION, toes, FROM);
        rabbitMessageListener.receiveMessageAndSend(mailData);
    }

    private MailData mailDataBuilder(Date date, MailTemplate template, List<String> to, String from) {
        return null;
//        return new MailData.builder(date, new User("Hayk", "Mkhitaryan"), template, to, from, Language.ENGLISH)
//                .setCc(null).setBc(null).setAttachment(null).build();
    }

    private Date getDate() {
        final String dateStr = "2013-02-04";

        SimpleDateFormat dateformat = new SimpleDateFormat("dd-M-yyyy");

        Date date = null;
        try {
            date = dateformat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
