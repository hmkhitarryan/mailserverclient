package com.egs.receiver;

import com.egs.configReader.MailConfigReader;
import com.egs.configReader.RabbiMQConfigReader;
import com.egs.mailSender.MailSender;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

/**
 * Created by haykmk on 8/16/2018.
 */
@EnableRabbit
@SpringBootApplication
public class Application implements RabbitListenerConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /* This bean is to read the properties file configs */
    @Bean
    public RabbiMQConfigReader applicationConfig() {
        return new RabbiMQConfigReader();
    }

    @Bean
    public MailConfigReader mailConfigReader() {
        return new MailConfigReader();
    }

    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(consumerJackson2MessageConverter());
        return factory;
    }

    @Override
    public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
    }

    @Bean
    JavaMailSenderImpl javaMailSender() {
        return new JavaMailSenderImpl();
    }

    @Bean
    MailSender mailSender() {
        return new MailSender();
    }

    @Bean(name = "messageSource")
    public MessageSource getMessageSource() {
        final ReloadableResourceBundleMessageSource resource = new ReloadableResourceBundleMessageSource();
        resource.setBasenames("classpath:validation", "classpath:messages");
        resource.setDefaultEncoding("UTF-8");

        return resource;
    }

}
