package com.egs.receiver.listener;

import com.egs.mailSender.MailSender;
import com.egs.model.MailData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

/**
 * A listener class which is responsible for getting
 * appropriate rabbitMQ messages as <code>MailData</code> object,
 * and sending email to the specified user.
 *
 * @author Hayk Mkhitaryan
 */
@Service
public class RabbitMessageListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMessageListener.class);
    private static final String QUEUE_SPECIFIC_NAME = "${app.queue.name}";

    @Autowired
    private MailSender mailSender;

    /**
     * Message listener for app
     *
     * @param mailData a user defined object used for deserialization of message
     */
    @RabbitListener(queues = QUEUE_SPECIFIC_NAME)
    public void receiveMessageAndSend(final MailData mailData) {
        LOGGER.info("Received message: {} from app queue.", mailData);
        try {
            LOGGER.info("Sending message to user...");
            mailSender.sendMail(mailData);
        } catch (HttpClientErrorException ex) {
            handleError(ex);
        } catch (Exception e) {
            LOGGER.error("Internal server error occurred in API call. Bypassing message requeue {}", e);
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }

    /**
     * Exception handler
     *
     * @param ex application specific exception to be handled
     */
    private void handleError(HttpClientErrorException ex) {
        if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
            LOGGER.info("Delay...");
            throw new RuntimeException();
        } else {
            throw new AmqpRejectAndDontRequeueException(ex);
        }
    }
}
