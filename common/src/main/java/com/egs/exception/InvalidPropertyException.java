package com.egs.exception;

/**
 * Thrown when an application attempts to use invalid property, like invalid email.
 *
 * @author Hayk Mkhitaryan
 */
public class InvalidPropertyException extends RuntimeException {
    public InvalidPropertyException(String message) {
        super(message);
    }
}
