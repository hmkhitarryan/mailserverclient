package com.egs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A domain entity class which is bound to request data,
 * which holds only user specific fields.
 *
 * @author Hayk Mkhitaryan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
class User {

    private Long id;

    @NotNull
    @Size(min = 2, message = "{Size.user.firstName}")
    private String firstName;


    @NotNull
    @Size(min = 2, message = "{Size.user.lastName}")
    private String lastName;
}
