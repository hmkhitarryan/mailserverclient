package com.egs.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * A DTO class, which will be bound to request's file,
 * which will hold <code>MultipartFile</code> inside.
 *
 * @author Hayk Mkhitaryan
 */
@Getter
@Setter
public class FileBucket {
    private MultipartFile file;
    private String comment;
    private String link;
}
