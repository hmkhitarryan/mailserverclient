package com.egs.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * A domain entity class which will hold attached data
 * to <code>MainData</code> object.
 *
 * @author Hayk Mkhitaryan
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
class Attachment {
    private Long id;
    private String link;
    private String comment;
    private Date insertDate;
}
