package com.egs.model;

import com.egs.constant.Language;
import com.egs.constant.MailTemplate;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * A domain entity class which is bound to request data,
 * which holds user and email specific fields.
 *
 * @author Hayk Mkhitaryan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class MailData {

    private Long id;

    //required fields
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateCreated;

    @NotNull
    private User user;

    @NotNull
    private MailTemplate template;

    @NotNull
    private List<String> to;

    @NotNull
    @Email
    @Size(min = 2, message = "{Size.mailData.from")
    private String from;

    @NotNull
    private Language language;

    //optional  parameters
    private List<String> cc;
    private List<String> bcc;
    private List<Attachment> attachments;
}
