package com.egs.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Locale;

/**
 * Enumeration of Languages.
 * <p> The Languages can be used as a required argument when sending http request to the controller.</p>
 *
 * @author Hayk Mkhitaryan
 */
@Getter
@AllArgsConstructor
public enum Language {
    ENGLISH("eng", Locale.ENGLISH),
    FRENCH("fr", Locale.FRENCH);

    public static Language nameOf(String name) {
        for (Language lang : values()) {
            if (lang.name.endsWith(name)) {
                return lang;
            }
        }
        return null;
    }

    private final String name;

    private final Locale locale;
}
