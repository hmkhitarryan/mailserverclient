package com.egs.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enumeration of Mail Templates.
 * <p> The Templates can be used as a required argument when sending http request to the controller.</p>
 *
 * @author Hayk Mkhitaryan
 */
@Getter
@AllArgsConstructor
public enum MailTemplate {
    REGISTRATION("registration.vm", "subject.registration"),
    PASSWORD_RESET("password-reset.vm", "subject.password.reset"),
    PASSWORD_CHANGED("password-changed.vm", "subject.password.changed");

    private final String viewName;

    private final String subjectKey;
}
