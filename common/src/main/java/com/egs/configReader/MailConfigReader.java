package com.egs.configReader;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

/**
 * A configuration reader class specific to email sending properties, which reads
 * configs from "application.properties" file.
 *
 * @author Hayk Mkhitaryan
 */
@PropertySource("classpath:application.properties")
@Getter
public class MailConfigReader {
    @Value("${support.email}")
    private String supportEmail;
    @Value("${mail.smtp.host}")
    private String mailSmtpHost;
    @Value("${mail.smtp.port}")
    private int mailSmtpPort;
    @Value("${mail.transport.protocol}")
    private String mailSmtpProtocol;
    @Value("${mail.smtp.auth}")
    private String mailSmtpAuth;
    @Value("${mail.smtp.starttls.enable}")
    private String mailSmtpStarttlsEnable;
    @Value("${mail.debug}")
    private String mailDebug;
    @Value("${mail.smtp.username}")
    private String username;
    @Value("${mail.smtp.password}")
    private String password;
}
