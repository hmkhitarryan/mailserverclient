package com.egs.configReader;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

/**
 * A configuration reader class specific to rabbitMQ properties, which reads
 * configs from "application.properties" file.
 *
 * @author Hayk Mkhitaryan
 */
@PropertySource("classpath:application.properties")
@Getter
public class RabbiMQConfigReader {
    // rabbitmq properties
    @Value("${app.exchange.name}")
    private String appExchange;
    @Value("${app.queue.name}")
    private String appQueue;
    @Value("${app.routing.key}")
    private String appRoutingKey;
}
