package com.egs.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A configuration class which is responsible to deserialize json <code>Date</code> filed,
 * coming in different formats from request.
 *
 * @author Hayk Mkhitaryan
 */
@JsonDeserialize(using = DateAndTimeDeserialize.class)
public class DateAndTimeDeserialize extends JsonDeserializer<Date> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date deserialize(JsonParser paramJsonParser, DeserializationContext paramDeserializationContext)
            throws IOException {
        final String str = paramJsonParser.getText().trim();
        try {
            return DATE_FORMAT.parse(str);
        } catch (ParseException ignored) {
        }
        return paramDeserializationContext.parseDate(str);
    }
}
