
Spring-boot web application to implement mail client and server logic.
Application is a multi module project and only two modules are microservices : "rabbitMQ-sender" and "receiver".
As there is no communication between modules(i.e. sending data to other service), it is not configured as a "Eureka Server".
The modules are working independently from each other.
The project has the ability of parsing 3 forms of templates : "REGISTRATION", "PASSWORD_RESET", "PASSWORD_CHANGED"(for demonstration purpose only "REGISTRATION" form is implemented)
and has the ability of sending the results in two languages: "ENGLISH", "FRENCH".

modules:
    common ->           holds configs and DTO objects, which are common for all other modules.It will extend in the future to have the ability to persist the messages in database.
    rabbitMQ-sender ->  gets mail data from rest endpoint in the body of request, binds it as a "MailData" object, validates, and sends to rabbitMQ.
    receiver ->         receives the data from rabbitMQ and sends to specified mail with the help of "mail-sender" module.
    mail-sender ->      gets MailData as an input parameter, initializes "Velocity" logic and sends the data to specified email with "Velocity Template".


Here are steps to configure and run the project (it is initially configured to send mail with an existing support email,
    but you can set your desired email settings in "application.properties" file).
    1. Run the "receiver" and "rabbitMQ-sender" modules.
    2. Create a json like this {
                               	"template": "REGISTRATION",
                               	"user": {
                               			"firstName": "Hayk",
                               			"lastName": "Mkhitaryan"
                               	 },
                               	"dateCreated": "2018-12-17 66",
                               	"to": ["hayk_84@mail.ru"],
                               	"from": "haykmkhitaryan84@gmail.com",
                               	"language": "ENGLISH"
                               }

    3. Send it with request body via any request making tool (i.e. Postman). Also here you can specify your desired credentials. All the fields here are required.
    4. Check your mentioned email to see the result




 