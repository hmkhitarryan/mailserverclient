package com.egs.mailSender;

import com.egs.configReader.MailConfigReader;
import com.egs.model.MailData;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.StringWriter;
import java.util.Properties;

/**
 * A sender class which gets <code>MailData</code> as input parameter
 * and sends email to user, the data of which are specified in <code>MailData</code>
 *
 * @author Hayk Mkhitaryan
 */
@Component
public class MailSender {
    private static final String CLASS_RESOURCE_LOADER_CLASS = "class.resource.loader.class";
    private static final String CLASS_NAME = "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader";
    private static final String TEMPLATES_DIR = "/templates/";
    private static final String CLASS = "class";
    private static final String USER = "user";
    private static final String MESSAGE = "message";
    private static final String EMPTY_STR = "";
    private static final String DEAR = "dear";

    @Autowired
    private MailConfigReader mailConfigReader;

    @Autowired
    MessageSource messageSource;

    /**
     * Send mail.
     *
     * @param mailData the data to publish
     */
    public void sendMail(MailData mailData) {
        processSend(mailData);
    }

    /**
     * Processes sending email.
     *
     * @param mailData the data to extract from
     */
    private void processSend(final MailData mailData) {
        StringWriter writer = null;
        try {
            writer = initVelocity(mailData.getTemplate().getViewName(), mailData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // to avoid effectively final variable issues.
        final StringWriter finalWriter = writer;
        final MimeMessagePreparator preparator = mimeMessage -> {
            final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setSubject(mailData.getTemplate().getSubjectKey());
            helper.setFrom(new InternetAddress(mailConfigReader.getSupportEmail()));
            helper.setTo(getAllRecipients(mailData));
            helper.setText(finalWriter != null ? finalWriter.toString() : EMPTY_STR, true);
        };
        final JavaMailSender javaMailSender = prepareJavaMail();
        javaMailSender.send(preparator);
    }

    /**
     * Get all Recipients.
     *
     * @param mailData the data to extract from
     * @return InternetAddresses to send email to
     */
    private InternetAddress[] getAllRecipients(MailData mailData) {
        final int size = mailData.getTo().size();
        final InternetAddress[] addresses = new InternetAddress[size];
        for (int i = 0; i < size; i++) {
            try {
                addresses[i] = new InternetAddress(mailData.getTo().get(i));
            } catch (AddressException e) {
                e.printStackTrace();
            }
        }
        return addresses;
    }

    /**
     * Initialize velocity template.
     *
     * @param mailData     the data to extract from
     * @param templateName the name of the template
     * @return StringWriter to be merged with
     */
    private StringWriter initVelocity(String templateName, MailData mailData) throws Exception {
        final VelocityEngine ve = new VelocityEngine();
        ve.setProperty(Velocity.RESOURCE_LOADER, CLASS);
        ve.setProperty(CLASS_RESOURCE_LOADER_CLASS, CLASS_NAME);
        ve.init();
        final Template t = ve.getTemplate(TEMPLATES_DIR + templateName);
        final VelocityContext context = new VelocityContext();
        final String dear = messageSource.getMessage("dear.label", null, mailData.getLanguage().getLocale());
        context.put(MESSAGE, messageSource.getMessage(mailData.getTemplate().getSubjectKey(), null, mailData.getLanguage().getLocale()));
        context.put(DEAR, dear);
        context.put(USER, mailData.getUser());
        final StringWriter writer = new StringWriter();
        t.merge(context, writer);
        return writer;
    }

    /**
     * Prepares JavaMailSender.
     *
     * @return JavaMailSenderImpl prepared object
     */
    private JavaMailSenderImpl prepareJavaMail() {
        final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailConfigReader.getMailSmtpHost());
        mailSender.setPort(mailConfigReader.getMailSmtpPort());
        mailSender.setUsername(mailConfigReader.getUsername());
        mailSender.setPassword(mailConfigReader.getPassword());
        final Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", mailConfigReader.getMailSmtpProtocol());
        props.put("mail.smtp.auth", mailConfigReader.getMailSmtpAuth());
        props.put("mail.smtp.starttls.enable", mailConfigReader.getMailSmtpStarttlsEnable());
        props.put("mail.debug", mailConfigReader.getMailDebug());

        return mailSender;
    }
}