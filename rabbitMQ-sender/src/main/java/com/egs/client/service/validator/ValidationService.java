package com.egs.client.service.validator;

import com.egs.exception.InvalidPropertyException;
import com.egs.model.MailData;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * A service class which validates <code>MailData</code> entity.
 *
 * @author Hayk Mkhitaryan
 */
@Service
public class ValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationService.class);

    private static final String AT = "@";

    /**
     * Validate email.
     *
     * @param mailData the emails of which to be validated
     */
    public void validate(MailData mailData) {
        if (isInvalidEmail(mailData.getTo()) || isInvalidEmail(Collections.singletonList(mailData.getFrom()))) {
            LOGGER.error("invalid email");
            throw new InvalidPropertyException("Any of the emails are invalid");
        }
    }

    /**
     * Validate email.
     *
     * @param email list to be validated
     * @return true if one of the list elements is invalid
     */
    private boolean isInvalidEmail(List<String> email) {
        boolean invalidEmail = false;
        for (String em : email) {
            if (isEmpty(em) || invalidEmail(em)) {
                invalidEmail = true;
                break;
            }

        }
        return invalidEmail;
    }

    /**
     * Check email.
     *
     * @param field to check the nullability
     * @return true if field is empty
     */
    private boolean isEmpty(String field) {
        return StringUtils.isEmpty(field);
    }

    /**
     * Check email.
     *
     * @param email to check
     * @return true if email is invalid
     */
    private boolean invalidEmail(String email) {
        return !email.contains(AT) || !email.contains(".");
    }


}
