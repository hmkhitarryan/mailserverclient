package com.egs.client;

import com.egs.configReader.RabbiMQConfigReader;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableRabbit
public class MailClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailClientApplication.class, args);
    }

    @Bean
    public RabbiMQConfigReader applicationConfig() {
        return new RabbiMQConfigReader();
    }
}
