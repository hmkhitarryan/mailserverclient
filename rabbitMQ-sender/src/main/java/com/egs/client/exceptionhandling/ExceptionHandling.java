package com.egs.client.exceptionhandling;


import com.egs.exception.InvalidPropertyException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.text.ParseException;

/**
 * An exception handling class,(ControllerAdvice)
 * which handles the specified exceptions, thrown in controller,
 * and returns the appropriate status codes to clients.
 *
 * @author Hayk Mkhitaryan
 */
@Order(Ordered.LOWEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionHandling {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<?> handleMethodArgumentNotValidException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @ExceptionHandler(ParseException.class)
    protected ResponseEntity<?> handleParseException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @ExceptionHandler(InvalidPropertyException.class)
    protected ResponseEntity<?> handleInvalidPropertyException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
