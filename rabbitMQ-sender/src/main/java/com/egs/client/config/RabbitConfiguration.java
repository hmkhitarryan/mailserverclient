package com.egs.client.config;

import com.egs.configReader.RabbiMQConfigReader;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * A configuration class specific to rabbitMQ properties.
 *
 * @author Hayk Mkhitaryan
 */
@Configuration
public class RabbitConfiguration {
    @Autowired
    private RabbiMQConfigReader rabbiMQConfigReader;

    @Bean
    public TopicExchange appExchange() {
        return new TopicExchange(rabbiMQConfigReader.getAppExchange());
    }

    @Bean
    public Queue appQueueSpecific() {
        return new Queue(rabbiMQConfigReader.getAppQueue());
    }

    @Bean
    public Binding declareBindingSpecific() {
        return BindingBuilder.bind(appQueueSpecific()).to(appExchange()).with(rabbiMQConfigReader.getAppRoutingKey());
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
