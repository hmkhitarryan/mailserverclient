package com.egs.client.sender;

import com.egs.configReader.RabbiMQConfigReader;
import com.egs.model.MailData;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * An implementation class of <code>MailDataSender</code> interface,
 * that is responsible for sending <code>MailData</code> to rabbitMQ.
 *
 * @author Hayk Mkhitaryan
 */
@Service
@RequiredArgsConstructor
public class MailDataSenderImpl implements MailDataSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailDataSenderImpl.class);

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbiMQConfigReader rabbiMQConfigReader;

    @Override
    public void sendMailData(MailData mailData) {
        LOGGER.info("Sending message...");
        rabbitTemplate.convertAndSend(rabbiMQConfigReader.getAppExchange(), rabbiMQConfigReader.getAppRoutingKey(), mailData);
    }
}
