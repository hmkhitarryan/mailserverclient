package com.egs.client.sender;

import com.egs.model.MailData;

/**
 * An interface, that is responsible for
 * sending <code>MailData</code> to any queue.
 *
 * @author Hayk Mkhitaryan
 */
public interface MailDataSender {

    /**
     * Send mail data to RabbitMQ server
     *
     * @param mailData data to be sent
     */
    void sendMailData(MailData mailData);
}
