package com.egs.client.controller;

import com.egs.client.sender.MailDataSender;
import com.egs.client.service.validator.ValidationService;
import com.egs.model.MailData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * A controller class which receives data from request,
 * binds to <code>MailData</code> entity, validates the data,
 * sends to rabbitMQ.
 *
 * @author Hayk Mkhitaryan
 */
@RestController
@RequestMapping("/api")
public class MailClientRestController {

    @Autowired
    private MailDataSender mailDataSender;

    @Autowired
    private ValidationService validationService;

    @PostMapping("/mails")
    public MailData sendMail(@Valid @RequestBody MailData mailData) {
        validationService.validate(mailData);
        mailDataSender.sendMailData(mailData);

        return mailData;
    }
}
